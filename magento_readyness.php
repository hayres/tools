<?php


/**
 * A simple fix for a shell execution on preg_match('/[0-9]\.[0-9]+\.[0-9]+/', shell_exec('mysql -V'), $version);
 * The only edit that was done is that shell_exec('mysql -V') was changed to mysql_get_server_info() because not all
 * systems have shell access. XAMPP, WAMP, or any Windows system might not have this type of access. mysql_get_server_info()
 * is easier to use because it pulls the MySQL version from phpinfo() and is compatible with all Operating Systems.
 * @link http://www.magentocommerce.com/knowledge-base/entry/how-do-i-know-if-my-server-is-compatible-with-magento
 * @author Magento Inc.
 */

function extension_check($extensions) {
    $fail = '';
    $pass = '';

    if(version_compare(phpversion(), '5.6.0', '<')) {
        $fail .= 'You need PHP 5.6.0 (or greater)'. PHP_EOL;
    } else {
        $pass .='You have PHP 5.6.0 (or greater)'. PHP_EOL;
    }

    if(!ini_get('safe_mode')) {
        $pass .='Safe Mode is off'. PHP_EOL;
        preg_match('/[0-9]\.[0-9]+\.[0-9]+/', shell_exec('mysql -V'), $version);

        if(version_compare($version[0], '4.1.20', '<')) {
            $fail .= 'You need MySQL 4.1.20 (or greater)' . PHP_EOL;
        } elseif (version_compare($version[0], '5.7.0', '>=')) {
            $pass .="You have MySQL $version[0].  You must have Version 5.6.x for Magento v1". PHP_EOL;
        } else {
            $pass .="You have MySQL $version[0] Version 5.6.X is the highest Version required". PHP_EOL;
        }
    } else { $fail .= 'Safe Mode is on.  Safe mode must be off.'.PHP_EOL;  }

    foreach($extensions as $extension) {
        if(!extension_loaded($extension)) {
            $fail .= 'You are missing the '.$extension.' extension'. PHP_EOL;
        } else{	$pass .= 'You have the '.$extension.' extension' . PHP_EOL;
        }
    }

    if($fail) {
        echo 'Your server does not meet the following requirements in order to install Magento.'. PHP_EOL;
        echo 'The following requirements failed, please contact your hosting provider in order to receive assistance with meeting the system requirements for Magento:'. PHP_EOL. PHP_EOL;
        echo $fail. PHP_EOL. PHP_EOL;
        echo 'The following requirements were successfully met:'. PHP_EOL. PHP_EOL;
        echo $pass. PHP_EOL;
    } else {
        echo 'Congratulations!Your server meets the requirements for Magento.'. PHP_EOL;
        echo $pass. PHP_EOL;

    }
}

extension_check(array(
    'curl',
    'dom',
    'gd',
    'hash',
    'iconv',
    'mcrypt',
    'pcre',
    'pdo',
    'pdo_mysql',
    'simplexml',
    'IonCube Loader',
    'imagick'
));

//phpinfo();

