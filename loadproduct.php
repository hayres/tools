<?php
/**
 * Created by PhpStorm.
 * User: hayres
 * Date: 24/04/2017
 * Time: 15:31
 */




require_once('../mapshop_magento/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Now you can run ANY Magento code you want

/* Get a product, get its bundled product and output jsondata.
*good for fixtures
 *
 */
$_product = Mage::getModel('catalog/product')->load(4171);


echo  Mage::helper('core')->jsonEncode($_product->getData()). PHP_EOL;


$bundled_product = new Mage_Catalog_Model_Product();
$bundled_product->load(4171);


$selectionCollection = $bundled_product->getTypeInstance(true)->getSelectionsCollection(
    $bundled_product->getTypeInstance(true)->getOptionsIds($bundled_product), $bundled_product
);

echo  Mage::helper('core')->jsonEncode($selectionCollection->getData()). PHP_EOL;

